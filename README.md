# Urban NaviGator Launch Files
Clone this repo under ~/autoware.ai/src

## Sensor Configuration
---
The main sensor for this implementation is a Velodyne VLP-16 lidar.

WIP: integrate the sylphase Infix-1 with the vehicle and use its output with NDT matching.

## Software Overview
---
### Localization
Localization is done primarly with the NDT matching software. This software uses a pointcloud map and a lidar to estimate the pose of the base_link.

### Control
The pure_pursuit node is used to take waypoints and generate a twist for the vehicle to perform.

### Planning
The lane_planner software packages is used with the ll2_global_planner package to generate paths based on a lanelet2 map and traverse this
path while applying appropriate traffic logic.

The astar_avoid and velocity_set nodes take the output of the lane_planner and perform any necessary obstacle avoidance before passing the final
set of waypoints to the pure_pursuit node.
